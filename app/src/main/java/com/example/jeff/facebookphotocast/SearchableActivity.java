package com.example.jeff.facebookphotocast;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jeff on 28/06/2015.
 */
public class SearchableActivity extends AppCompatActivity{

    private class PersonIdPair{

        private String name;
        private String id;
        private String imageUrl;

        public PersonIdPair(String name, String id, String imageUrl){
            this.name = name;
            this.id = id;
            this.imageUrl = imageUrl;
        }

        public PersonIdPair(PersonIdPair pair){
            this.name = pair.name;
            this.id = pair.id;
            this.imageUrl = pair.imageUrl;
        }

        public String getName(){
            return this.name;
        }

        public String getId(){
            return this.id;
        }

        public String getImageUrl(){
            return this.imageUrl;
        }

        @Override
        public String toString(){
            return this.name;
        }

    }

    AccessToken accessToken;
    SearchView searchView;
    ListView listView ;
    ArrayList<PersonIdPair> allFriends = new ArrayList<PersonIdPair>();
    String currentText = "";
    //ArrayList<PersonIdPair> currentList = new ArrayList<PersonIdPair>();
    ArrayList<String> currentNameList = new ArrayList<String>();
    ArrayList<String> currentUrlList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        accessToken = Settings.get().facebook();
        listView = (ListView) findViewById(R.id.list);
        getFriends(null);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void getFriends(String after){
        Bundle param = new Bundle();
        if (after == null)
            param = null;
        else
            param.putString("after", after);
        new GraphRequest(
            accessToken,
            "/me/taggable_friends",
            param,
            HttpMethod.GET,
            new GraphRequest.Callback() {
                public void onCompleted(GraphResponse response) {
                    try {
                        for (int i = 0; i < response.getJSONObject().getJSONArray("data").length(); i++) {
                            String name = null;
                            String id = null;
                            String imageUrl = null;
                            try {
                                name = response.getJSONObject().getJSONArray("data").getJSONObject(i).getString("name");
                                id = response.getJSONObject().getJSONArray("data").getJSONObject(i).getString("id");
                                imageUrl = response.getJSONObject().getJSONArray("data").getJSONObject(i).getJSONObject("picture").getJSONObject("data").getString("url");
                            }
                            catch (JSONException e) {}
                            allFriends.add(new PersonIdPair(name, id, imageUrl));
                        }
                        if (!response.getJSONObject().isNull("paging"))
                            getFriends(response.getJSONObject().getJSONObject("paging").getJSONObject("cursors").getString("after"));
                        else
                            Toast.makeText(getApplicationContext(),Integer.toString(allFriends.size()),Toast.LENGTH_SHORT).show();
                    }
                    catch(Exception e){}
                }
            }
        ).executeAsync();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchable, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Search...");
        setupSearchView(searchItem);
        return super.onCreateOptionsMenu(menu);
    }

    public void setAdapter(){
        if(!currentText.equals("")) {
            String[] friendNames;
            String[] friendUrl;
            if(currentNameList.size() <= 15) {
                friendNames = currentNameList.toArray(new String[0]);
                friendUrl = currentUrlList.toArray(new String[0]);
            }
            else{
                friendNames = currentNameList.subList(0,15).toArray(new String[0]);
                friendUrl = currentUrlList.subList(0,15).toArray(new String[0]);
            }
            CustomListAdapter adapter = new CustomListAdapter(this, friendNames, friendUrl);
            listView.setAdapter(adapter);
        }
        else
            listView.setAdapter(new CustomListAdapter(this,new String[]{""},new String[]{""}));
    }

    private void setupSearchView(MenuItem searchItem) {

        if (true) {
            searchView.setIconifiedByDefault(false);
        } else {
            searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
                    | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                currentText = newText;
                if(!currentText.equals("")) {
                    currentNameList = new ArrayList<String>();
                    currentUrlList = new ArrayList<String>();
                    for (int i = 0; i < allFriends.size(); i++) {
                        if (allFriends.get(i).getName().toUpperCase().contains(currentText.toUpperCase())) {
                            currentNameList.add(allFriends.get(i).getName());
                            currentUrlList.add(allFriends.get(i).getImageUrl());
                        }
                    }
                }
                setAdapter();
                return true;
            }
        });
    }
}
