package com.example.jeff.facebookphotocast;

public class ViewModel {
    private String text;
    private String image;
    private String id;
    private String extraParam;
    private String extraParam2;

    public ViewModel(String... params){
        this.image = "";
        this.id = "";
        this.image = "";
        this.extraParam = "";
        this.extraParam2 = "";
        if(params.length > 0)
            this.text = params[0];
        if(params.length > 1)
            this.image = params[1];
        if(params.length > 2)
            this.id = params[2];
        if(params.length > 3)
            this.extraParam = params[3];
        if(params.length > 4)
            this.extraParam2 = params[4];
    }

    public String getText() {
        return text;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getExtraParam(){ return extraParam; }

    public String getExtraParam2(){ return extraParam2; }
}
