package com.example.jeff.facebookphotocast;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

import static android.app.PendingIntent.getActivity;


public class Photo extends AppCompatActivity {

    private Toolbar toolbar;
    private String albumName;
    private String photoURL;
    private ArrayList<String> bigUrls;
    private ArrayList<String> tempBigUrls;
    private ImageView mainPicture;

    private static AlertDialog d ;

    private int currentIndex;
    private boolean playing;
    private int delay = 4;
    private boolean fillScreen = true;

    private Handler handler;
    private Runnable r;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_photo);

        albumName = getIntent().getStringExtra("albumName");
        photoURL = getIntent().getStringExtra("photoURL");
        Gson gson = new Gson();
        bigUrls = gson.fromJson(getIntent().getStringExtra("photoUrls"),ArrayList.class);
        for (int i = 0; i < bigUrls.size(); i++) {
            if (bigUrls.get(i).equals(photoURL)) {
                currentIndex = i;
                break;
            }
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.drawable.alpha_gradient);
        setSupportActionBar(toolbar);
        this.setTitle(albumName);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mainPicture = (ImageView)findViewById(R.id.mainPicture);

        Settings.get().sendChromecast("{\"action\":\"playlist\", \"data\":" + gson.toJson(bigUrls) + ", \"from\":" + currentIndex + ", \"context\":{\"album\":\"" + albumName + "\", \"author\":\"Ivan D'halluin\"}}");

        Button nextButton = (Button)findViewById(R.id.NexButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });

        Button playButton = (Button)findViewById(R.id.PlayButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play();
            }
        });

        Button prevButton = (Button)findViewById(R.id.PrevButton);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentIndex--;
                if (currentIndex == -1)
                    currentIndex = bigUrls.size() - 1;
                refreshPhoto();
                Settings.get().sendChromecast("{\"action\":\"prev\"}");
            }
        });

        Button goBackToFirstButton = (Button)findViewById(R.id.goBackToFirstButton);
        goBackToFirstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentIndex = 0;
                refreshPhoto();
                Settings.get().sendChromecast("{\"action\":\"first\"}");
            }
        });

        Button shuffleButton = (Button)findViewById(R.id.shuffleButton);
        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempBigUrls == null) {
                    v.setBackgroundResource(R.drawable.ic_sort_by_alpha_white);
                    tempBigUrls = new ArrayList<String>(bigUrls);
                    Collections.shuffle(bigUrls);
                }
                else {
                    v.setBackgroundResource(R.drawable.ic_shuffle_white);
                    bigUrls = new ArrayList<String>(tempBigUrls);
                    tempBigUrls = null;
                }
                for(int i = 0; i < bigUrls.size();i++){
                    if(bigUrls.get(i).equals(photoURL)) {
                        currentIndex = i;
                        break;
                    }
                }
                Gson gson = new Gson();
                Settings.get().sendChromecast("{\"action\":\"playlist\", \"data\":" + gson.toJson(bigUrls) + ", \"from\":" + currentIndex + ", \"context\":{\"album\":\"" + albumName + "\", \"author\":\"Ivan D'halluin\"}}");
            }
        });

        Button toggleButton = (Button)findViewById(R.id.toggleButton);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fillScreen) {
                    v.setBackgroundResource(R.drawable.ic_crop_white);
                    fillScreen = false;
                    Settings.get().sendChromecast("{\"action\":\"fill\"}");
                } else {
                    v.setBackgroundResource(R.drawable.ic_fullscreen_white);
                    fillScreen = true;
                    Settings.get().sendChromecast("{\"action\":\"fill\"}");
                }
            }
        });

        Button delayButton = (Button)findViewById(R.id.delayButton);
        delayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();
            }
        });

        Picasso.with(getApplicationContext()).load(photoURL).into(mainPicture);
        putInCache();

        if (getIntent().getBooleanExtra("playing", false)) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    play();
                }
            }, 3000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void play() {
        Button v = (Button)findViewById(R.id.PlayButton);
        if (playing) {
            v.setBackgroundResource(R.drawable.ic_play_arrow_white);
            playing = false;
            handler.removeCallbacks(r);
            Settings.get().sendChromecast("{\"action\":\"pause\"}");
        }
        else {
            v.setBackgroundResource(R.drawable.ic_pause_white);
            handler = new Handler();
            r = new Runnable() {
                public void run() {
                    next();
                    handler.postDelayed(this, delay * 1000);
                }
            };
            handler.postDelayed(r, delay * 1000);
            playing = true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void next() {
        currentIndex++;
        if (currentIndex == bigUrls.size())
            currentIndex = 0;
        Settings.get().sendChromecast("{\"action\":\"next\"}");
        refreshPhoto();
    }

    public void putInCache() {
        if (currentIndex == 0)
            Picasso.with(getApplicationContext()).load(bigUrls.get(bigUrls.size()-1)).fetch();
        else
            Picasso.with(getApplicationContext()).load(bigUrls.get(currentIndex-1)).fetch();
        if (currentIndex == bigUrls.size()-1)
            Picasso.with(getApplicationContext()).load(bigUrls.get(0)).fetch();
        else
            Picasso.with(getApplicationContext()).load(bigUrls.get(currentIndex+1)).fetch();
    }

    public void refreshPhoto() {
        photoURL = bigUrls.get(currentIndex);
        putInCache();
        Picasso.with(getApplicationContext()).load(photoURL).into(mainPicture);
    }

    public void show() {
        final AlertDialog d = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog)).create();
        d.setTitle("Choose a delay time");
        View view = getLayoutInflater().inflate(R.layout.dialog, null);
        d.setView(view);
        final NumberPicker np = (NumberPicker)view.findViewById(R.id.numberPicker1);
        TextView tv = (TextView)view.findViewById(R.id.info);
        np.setMaxValue(10);
        np.setMinValue(5);
        np.setWrapSelectorWheel(false);
        d.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                delay = np.getValue();
                d.dismiss();
            }
        });
        d.setButton(Dialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                d.dismiss();
            }
        });
        d.show();
    }

    @Override
    public void onStop(){
        super.onStop();
        if(handler != null)
            handler.removeCallbacks(r);
    }
}
