package com.example.jeff.facebookphotocast;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thibault on 15/05/2015.
 */
public class Album extends Fragment implements RecyclerViewAdapter.OnItemClickListener{

    private Toolbar toolbar;
    private AccessToken accessToken;
    private static List<ViewModel> items = new ArrayList<>();
    private ArrayList<String> bigUrls = new ArrayList<String>();
    private RecyclerView recyclerView;
    private View view;
    private String albumId;
    private String albumName;
    private String albumAuthor;
    private String coverUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.activity_album, container, false);

        Bundle args = getArguments();
        accessToken = Settings.get().facebook();

        albumId = args.getString("albumId");
        albumName = args.getString("albumName");
        albumAuthor = args.getString("albumAuthor");
        coverUrl = args.getString("coverUrl");

        ((TextView) view.findViewById(R.id.album)).setText(albumName);
        ((TextView) view.findViewById(R.id.author)).setText(albumAuthor);

        items.clear();
        bigUrls.clear();
        getPhotos(null);

        ImageButton playButton = (ImageButton)view.findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToOnePhoto(null, items.get(0).getId(), true);
            }
        });

        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ImageView cover = (ImageView) view.findViewById(R.id.cover);
        Picasso.with(getActivity()).load(coverUrl).into(cover);
    }

    public static Album newInstance(final String albumId, final String albumName, final String albumAuthor, final String coverUrl) {
        final Album album = new Album();
        final Bundle bundle = new Bundle();
        bundle.putString("albumId", albumId);
        bundle.putString("albumName", albumName);
        bundle.putString("albumAuthor", albumAuthor);
        bundle.putString("coverUrl", coverUrl);
        album.setArguments(bundle);
        return album;
    }

    public void getPhotos(String next) {
        Bundle param = new Bundle();
        if (next == null)
            param = null;
        else
            param.putString("after", next);
        new GraphRequest(
                accessToken,
                "/" + albumId + "/photos",
                param,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            JSONArray data = response.getJSONObject().getJSONArray("data");
                            for (int j = 0; j < data.length(); j++) {
                                JSONObject photo = data.getJSONObject(j);
                                JSONArray jsonImages = photo.getJSONArray("images");
                                int smallest = jsonImages.length() - 1;
                                String url = jsonImages.getJSONObject(smallest).getString("source");
                                // optimize given picture to fit screen (both phone and chromecast) TODO: VERIFY
                                int size = 0;
                                if (Integer.parseInt(jsonImages.getJSONObject(0).getString("height")) > 720 )
                                    size = 1;
                                String bigURL = jsonImages.getJSONObject(size).getString("source");
                                bigUrls.add(bigURL);
                                items.add(new ViewModel("", url, bigURL));
                            }
                            if(!response.getJSONObject().isNull("paging"))
                                getPhotos(response.getJSONObject().getJSONObject("paging").getJSONObject("cursors").getString("after"));
                            initRecyclerView(); //TODO: Avoid bad scroll
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(items,false);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    public void switchToOnePhoto(View view, String photoURL, boolean playing){
        Intent intent = new Intent(getActivity(), Photo.class);
        intent.putExtra("photoURL", photoURL);
        intent.putExtra("albumName", albumName);
        Gson gson = new Gson();
        intent.putExtra("photoUrls", gson.toJson(bigUrls));
        intent.putExtra("playing", playing);
        startActivity(intent);
    }

    @Override
    public void onItemClick(View view, ViewModel viewModel) {
        switchToOnePhoto(view, viewModel.getId(),false);
    }
}
