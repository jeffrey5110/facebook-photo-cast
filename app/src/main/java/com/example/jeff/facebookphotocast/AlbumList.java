package com.example.jeff.facebookphotocast;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AlbumList extends Fragment implements RecyclerViewAdapter.OnItemClickListener {

    static final int PICK_ACCESS_TOKEN = 1;

    private List<String> searchItems;

    private static List<ViewModel> items = new ArrayList<>();
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private AccessToken accessToken;
    private View view;
    CallbackManager callbackManager;

    String id;
    final List<String> albumIds = new ArrayList<>();
    final List<String> albumNames = new ArrayList<>();
    final List<String> albumAuthors = new ArrayList<>();
    final List<String> albumCoverIds = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager = CallbackManager.Factory.create();
        view = inflater.inflate(R.layout.activity_album_list, container, false);

        id = "me";
        if(getActivity().getIntent().getStringExtra("id") != null)
            id = getActivity().getIntent().getStringExtra("id");

        accessToken = Settings.get().facebook();
        Bundle param = new Bundle();
        param.putBoolean("redirect",false);
        param.putString("fields","cover");

        new GraphRequest(
                accessToken,
                "/" + id,
                param,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            ImageView cover = (ImageView) view.findViewById(R.id.cover);
                            Picasso.with(getActivity()).load(response.getJSONObject().getJSONObject("cover").getString("source")).into(cover);
                        }
                        catch(Exception e){}
                    }
                }
        ).executeAsync();

        new GraphRequest(
                accessToken,
                "/" + id,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            ((TextView) view.findViewById(R.id.user)).setText(response.getJSONObject().getString("name"));
                        }
                        catch(Exception e){}
                    }
                }
        ).executeAsync();

        if (items.size() == 0)
            getAlbums(null);
        else
            initRecyclerView();

        return view;
    }

    private void getAlbums(String next) {
        Bundle param = new Bundle();
        if (next == null)
            param = null;
        else
            param.putString("after", next);
        new GraphRequest(
                accessToken,
                "/" + id + "/albums/",
                param,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            JSONObject albums = response.getJSONObject();
                            JSONArray data = albums.getJSONArray("data");
                            for (int j = 0; j < data.length(); j++) {
                                JSONObject album = data.getJSONObject(j);
                                final String albumName = album.getString("name");
                                final String albumAuthor = album.getJSONObject("from").getString("name");
                                String albumCoverId = album.getString("cover_photo");
                                String albumId = album.getString("id");
                                albumIds.add(albumId);
                                albumNames.add(albumName);
                                albumAuthors.add(albumAuthor);
                                albumCoverIds.add(albumCoverId);
                            }

                            if(!response.getJSONObject().isNull("paging"))
                                getAlbums(response.getJSONObject().getJSONObject("paging").getJSONObject("cursors").getString("after"));
                            else {
                                setAlbumCoverPicture();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(items,true);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    public void setAlbumCoverPicture(){
        final String urls[] = new String[albumIds.size()];
        final String bigUrls[] = new String[albumIds.size()];
        final int nb = albumCoverIds.size();
        for (int i = 0; i < nb; i++) {
            final int k = i;
            new GraphRequest(
                    accessToken,
                    "/" + albumCoverIds.get(i) + "/",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            try {
                                JSONArray jsonImages = response.getJSONObject().getJSONArray("images");
                                int smallest = jsonImages.length() - 1;
                                String url = jsonImages.getJSONObject(smallest).getString("source");
                                String bigUrl = jsonImages.getJSONObject(0).getString("source");
                                urls[k] = url;
                                bigUrls[k] = bigUrl;
                                boolean finished = true;
                                for (int j = 0; j < albumNames.size(); j++) {
                                    if(urls[j] == null)
                                        finished = false;
                                }
                                if (finished) {
                                    for (int j = 0; j < urls.length; j++)
                                        items.add(new ViewModel(albumNames.get(j),urls[j],albumIds.get(j),bigUrls[j],albumAuthors.get(j)));
                                }
                                initRecyclerView();
                                if (k == nb - 1)
                                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
        }
    }

    public void switchToAlbums(View view, String albumId, String albumName, String coverUrl, String albumAuthor){
        Intent intent = new Intent(getActivity(), AlbumUser.class);
        Gson gson = new Gson();
        intent.putExtra("albumId", albumId);
        intent.putExtra("albumName", albumName);
        intent.putExtra("coverUrl", coverUrl);
        intent.putExtra("albumAuthor", albumAuthor);
        startActivityForResult(intent,PICK_ACCESS_TOKEN);
    }

    @Override
    public void onItemClick(View view, ViewModel viewModel) {
        switchToAlbums(view, viewModel.getId(), viewModel.getText(), viewModel.getExtraParam(), viewModel.getExtraParam2());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        //findViewById(R.id.loading).setVisibility(View.GONE);
    }
}
